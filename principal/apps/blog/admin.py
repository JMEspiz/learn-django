from django.contrib import admin
from principal.apps.blog.models import Post, Author

# Register your models here.
''' Clase que extiende del Model Admin, para representar los datos
    en el Admin de Django '''
class PostAdmin(admin.ModelAdmin):
    ''' aqui se define los campos a mostrar | tupla '''
    list_display = ('post_created_date', 'post_title', 'author', 'post_slug')

    ''' campos que son buscables | lista '''
    search_fields = ['post_title', 'author', 'post_slug']

    ''' orden de los campos a mostrar | tupla '''
    ordering = ('post_created_date', 'post_title', 'author', 'post_slug')

    ''' estilo del formulario del CRUD
        Estructura dentro de la lista:
            ('nombre', {
                fields:('campo1','campo2','campo3',)
            })
    '''

    fieldsets = [
        ('Datos de Fecha', {'fields':['post_created_date']}),
        ('Datos del Articulo', {'fields':['post_title','post_body','post_slug']}),
        ('Datos de Autor', {'fields':['author']})
    ]


''' Registro de los Modelos en el Admin '''

admin.site.register(Post, PostAdmin)
admin.site.register(Author)

''' se puede usar un decorador así:
    @admin.register(Clase1, Clase2, Clase3) '''
