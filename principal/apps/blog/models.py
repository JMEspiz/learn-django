from django.db import models
import datetime
from django.utils import timezone

# Create your models here.

class Author(models.Model):
    ''' Clase para la tabla Autores '''
    author_name = models.CharField(max_length = 120, verbose_name = 'Autor',
                                   blank = True, default = 'Administrador')
    author_email = models.EmailField(verbose_name = 'Email', blank = True)

    class Meta:
        verbose_name_plural = 'Autores' # Nombre para el admin de Django

    def __str__(self):
        return self.author_name

class Post(models.Model):
    '''clase para la tabla Articulos'''
    author = models.ForeignKey(Author)
    post_title = models.CharField(max_length = 200, verbose_name = 'Titulo')
    post_body = models.TextField(verbose_name = 'Contenido')
    post_slug = models.CharField(max_length = 100, verbose_name = 'Etiquetas',
                                 blank = True)
    post_created_date = models.DateField(default = timezone.now(),
                                             verbose_name = 'Creado el',
                                             blank = True)

    class Meta:
        '''clase que permite sobreescribir valores para el admin'''
        verbose_name_plural = 'Articulos'
        ''' Orden en el que se veran en el admin '''
        ordering = ('post_created_date', 'post_title', 'author', 'post_slug')

    def __str__(self):
        return self.post_title
