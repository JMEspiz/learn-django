from django.conf.urls import patterns, url
from .views import *

#URLs para la App blog

urlpatterns = patterns('principal.apps.blog.views',
    #url(expresion regular, 'defname', name='nombre')
    #url(expresion regular, Clase.as_view(). name='nombre')
    url(r'^$', 'index', name='inicio'),
    url(r'^blog', 'index_blog', name='inicio_blog'),
)
