from django.shortcuts import render, render_to_response

# Create your views here.
def index(request):
    # retornar temppalte (ruta relativa, contexto)
    return render_to_response('principal/index.html', {})

def index_blog(request):
    # retornar temppalte (ruta relativa, contexto)
    return render_to_response('blog/index.html', {})
